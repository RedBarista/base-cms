<?php require_once("../includes/functions.php"); ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div class="container">
        <nav id="navigation">
            <h2>Navigation</h2>
        </nav>
        <div id="page">
            <h2>Admin Menu</h2>
            <p>Welcome to the admin area.</p>
            <ul>
                <li><a href="manage_content.php">Manage Content</a></li>
                <li><a href="manage_admins.php">Manage Admins</a></li>
                <li><a href="logout.php">Logout</a></li>
            </ul>
        </div>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>