<?php
    require_once("../includes/sessions.php");
    require_once("../includes/db_connection.php");
    require_once("../includes/functions.php");

    //Work
    if (isset($_POST['submit'])) {
        // Process the form

        $menu_name =  mysql_prep($_POST['menu_name']);
        $position = (int) $_POST['position'];
        $visible = (int) $_POST['visible'];

        // Database query
        $query =  "INSERT INTO subjects (";
        $query .= "menu_name, position, visible";
        $query .= ") VALUES (";
        $query .= "'{$menu_name}', {$position}, {$visible}";
        $query .= ")";

        $result = mysqli_query($connection, $query);

        // test if there is s query error
        if ($result) {
            //$_SESSION["message"] = "Subject created.";
            $_SESSION["message"] = "Subject created.";
            redirect_to("manage_content.php");
        } else {
            $_SESSION["message"] = "Subject creation failed.";
            redirect_to("new_subject.php");
        }


    } else {
        redirect_to("new_subject.php");
    }

    //End
    if (isset($connection)){ mysqli_close($connection); }
?>