<?php require_once("../includes/sessions.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php include("../includes/layouts/header.php"); ?>
<?php find_selected_page()?>

    <section id="main">
        <div class="container">
            <nav id="navigation">
                <h2>Navigation</h2>
                <?php echo navigation($current_subject, $current_page);?>
            </nav>
            <div id="page">
                <?php
                    echo message();
                ?>
                <h2>Create Subject</h2>
                <form method="post" action="create_subject.php">
                    <p><label>Menu name: <input type="text" name="menu_name"/></label></p>
                    <p><label>Position: 
                            <select name="position" id="">
                                <?php
                                    $subject_set = find_all_subjects();
                                    $subject_count = mysqli_num_rows($subject_set);
                                    for ($count = 1; $count <= ($subject_count + 1); $count++) {
                                        echo "<option value=\"{$count}\">{$count}</option>";
                                    }
                                ?>
                            </select></label>
                    </p>
                    <p>Visible:
                        <label><input type="radio" name="visible" value="0"/> No</label>
                        <label><input type="radio" name="visible" value="1"/> Yes</label>
                    </p>
                    <input class="btn" value="Create Subject" name="submit" type="submit"/>
                </form>
                <br />
                <a href="manage_content.php">Cancel</a>
            </div>
        </div>
    </section>

<?php include("../includes/layouts/footer.php"); ?>