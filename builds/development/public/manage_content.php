<?php require_once("../includes/sessions.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php include("../includes/layouts/header.php"); ?>
<?php find_selected_page()?>

<section id="main">
    <div class="container">
        <nav id="navigation">
            <h2>Navigation</h2>
            <?php echo navigation($current_subject, $current_page);?>
            <br/>
            <a href="new_subject.php">+ Add a subject</a>
        </nav>
        <div id="page">
            <?php echo message(); ?>
            <?php if ($current_subject) {?>
                <h2>Manage Subject</h2>
                <h1>Menu name: <?php echo $current_subject["menu_name"];?></h1>

            <?php } elseif ($current_page) {?>
                <h2>Manage Page</h2>
                <h1>Menu name: <?php echo $current_page["menu_name"];?></h1>

            <?php } else {?>
                Please select a subject or a page.
            <?php } ?>
        </div>
    </div>
</section>

<?php include("../includes/layouts/footer.php"); ?>